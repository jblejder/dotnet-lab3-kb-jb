﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class ObslugaPoczty
    {
        SmtpClient client;
        MailMessage mail;
        String from;


        public ObslugaPoczty()
        {
            from = "platfomry.net@gmail.com";
            client = new SmtpClient();
            client.Credentials = new NetworkCredential(from, "1234polibuda");
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
        }

        ~ObslugaPoczty()
        {
            mail.Dispose();
        }

        public void PrepareMail(String to, String body)
        {
            mail = new MailMessage();
            mail.To.Add(to);
            mail.From = new MailAddress(from, ".Net Blejder&Brenzak", Encoding.UTF8);
            mail.Subject = "Wiadomosc z zajec .Net";
            mail.SubjectEncoding = Encoding.UTF8;
            mail.Body = body + "\n\nWiadomosc wyslana w ramach zajec platfomry .net i jawa";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.High;
            mail.Attachments.Add(new Attachment("file.jpg"));
        }

        public bool Send()
        {
            if (mail == null)
                return false;
            try
            {
                client.Send(mail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
    }
}
