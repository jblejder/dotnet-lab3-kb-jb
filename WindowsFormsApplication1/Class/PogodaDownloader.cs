﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class Coord
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }

    public class Sys
    {
        public double message { get; set; }
        public string country { get; set; }
        public int sunrise { get; set; }
        public int sunset { get; set; }
    }

    public class Weather
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }

    public class Main
    {
        public double temp { get; set; }
        public double temp_min { get; set; }
        public double temp_max { get; set; }
        public double pressure { get; set; }
        public double sea_level { get; set; }
        public double grnd_level { get; set; }
        public int humidity { get; set; }
    }

    public class Wind
    {
        public double speed { get; set; }
        public double deg { get; set; }
    }

    public class Clouds
    {
        public int all { get; set; }
    }

    public class Pogoda
    {
        public Coord coord { get; set; }
        public Sys sys { get; set; }
        public List<Weather> weather { get; set; }
        public string @base { get; set; }
        public Main main { get; set; }
        public Wind wind { get; set; }
        public Clouds clouds { get; set; }
        public int dt { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int cod { get; set; }
    }



    static class PogodaDownloader
    {
        public static Pogoda PobierzPogode(string miasto)
        {
            string json;
            try
            {
                json = (new WebClient()).DownloadString(@"http://api.openweathermap.org/data/2.5/weather?q=" + miasto + @",pl");
            }
            catch (Exception e)
            {
                throw e;
            }
            return JsonConvert.DeserializeObject<Pogoda>(json);
        }

        public static byte[] PobierzObrazek(Pogoda pogoda)
        {
            string adres = "http://openweathermap.org/img/w/";
            string iconId = pogoda.weather[0].icon;
            byte[] obraz = (new WebClient()).DownloadData(adres + iconId + ".png");
            return obraz;
        }

    }
}
