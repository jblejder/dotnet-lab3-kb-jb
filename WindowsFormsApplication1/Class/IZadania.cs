﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    
    public interface IZadanie
    {
        int Id { get; set; }
        int taskID { get; set; }
        [NotMapped]
        Paczka dane { get; set; }

        bool RobRobote();
    }
    
    [Serializable]
    public class ZnajdzObraz : IZadanie
    {
        public int Id { get; set; }
        public int taskID { get; set; }
        public string url { set; get; }
        public string haslo { set; get; }

        [NotMapped]
        public Paczka dane { get; set; }

        public bool RobRobote()
        {
            ObslugaWWW obslugaWWW = new ObslugaWWW();
            obslugaWWW.LoadHtml(url);
            if (obslugaWWW.FindByAlt(haslo))
            {
                dane = new Paczka();
                try
                {
                    dane.image = obslugaWWW.DownloadFoundFile();
                }
                catch (Exception e)
                {
                    throw e;
                }
                dane.textData = "Haslo: "+haslo + "\nStrona: " + obslugaWWW.foundAdress;
                return true;

            }
            return false;
        }
        public Paczka PobierzRezultat()
        {
            return dane;
        }
        public override string ToString()
        {
            return "Znajdz obraz z podpisem: " + haslo + " Na stronie: " + url;
        }

    }
    [Serializable]
    public class SprawdzPogode : IZadanie
    {
        public int Id { get; set; }
        public int taskID { get; set; }
        public string miasto { get; set; }
        public int minTemp { get; set; }

        [NotMapped]
        public Paczka dane { get; set; }

        public bool RobRobote()
        {
            Pogoda pogoda;
            byte[] obraz;
            try
            {
                pogoda = PogodaDownloader.PobierzPogode(miasto);
                pogoda.main.temp -= 272.15; 
                if (pogoda.main.temp < minTemp)
                    return false;
                obraz = PogodaDownloader.PobierzObrazek(pogoda);
            }
            catch (Exception e)
            {
                throw e;
            }
            dane = new Paczka()
            {
                textData = "Pogoda dla miasta " + miasto +
                    "\nPredkosc wiatru: " + pogoda.wind.speed +
                    "\nTemp: " + pogoda.main.temp +
                    "\nCisnienie: " + pogoda.main.pressure,
                image = obraz
            };

            return true;
        }
        public override string ToString()
        {
            return "Temperatura w \"" + miasto + "\" wieksza niz; \"" + minTemp + "\"";
        }
    }
    [Serializable]
    public class PokazWOknie : IZadanie
    {
        public int Id { get; set; }
        public int taskID { get; set; }
        [NotMapped]
        public Paczka dane { get; set; }

        public bool RobRobote()
        {
            Form2 m = new Form2();
            m.WprowadzWazneDane(dane);
            Form1.singleton.Invoke((MethodInvoker)delegate()
            {
                m.Show();
            });
            

            return true;
        }

        public override string ToString()
        {
            return "Pokaz w oknie";
        }
    }
    [Serializable]
    public class WyslijMaila : IZadanie
    {
        public int Id { get; set; }
        public int taskID { get; set; }
        public string mail { set; get; }
        public bool udawajZeWysylasz { set; get; }
        public string url { get; set; }
        [NotMapped]
        public Paczka dane { get; set; }

        public bool RobRobote()
        {
            if (!udawajZeWysylasz)
            {
                
                    
                File.WriteAllBytes("file.jpg", dane.image);
                ObslugaPoczty poczta = new ObslugaPoczty();
                
                try
                {
                    poczta.PrepareMail(mail, dane.textData);   
                }
                catch (Exception exc)
                {
                    throw exc;
                }

                if (poczta.Send())
                {
                    return true;
                }
                else
                    return false;

            }
            else
            {
                return true;
            }
        }

        public override string ToString()
        {
            return "Wyslij maila na: " + mail;
        }
    }
}
