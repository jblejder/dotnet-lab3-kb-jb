﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel;
using System.Windows.Forms;


namespace WindowsFormsApplication1
{
    static class BazaDanych
    {
        public static BindingList<JTTT> OdczytajElementy()
        {
            BindingList<JTTT> list = new BindingList<JTTT>();
            JTTT zadanie;

            try
            {
                using (var ctx = new BazaContext())
                {
                   
                    foreach (var x in ctx.ZnajdzObraz)
                    {
                        foreach(var y in ctx.WyslijMaila){
                            if (x.taskID == y.taskID)
                            {;
                                zadanie = new JTTT() { jesliTo = x, toTo = y };
                                list.Add(zadanie);
                                break;
                            }
                        }
                        foreach (var y in ctx.PokazWOknie)
                        {
                            if (x.taskID == y.taskID)
                            {
                                zadanie = new JTTT() { jesliTo = x, toTo = y };
                                list.Add(zadanie);
                                break;
                            }
                        }
                    }
                  
                    foreach (var x in ctx.SprawdzPogode)
                    {
                   
                        foreach (var y in ctx.WyslijMaila)
                        {
                            if (x.taskID == y.taskID)
                            {
                                zadanie = new JTTT() { jesliTo = x, toTo = y };
                                list.Add(zadanie);
                                break;
                            }
                        }
                        foreach (var y in ctx.PokazWOknie)
                        {
                            if (x.taskID == y.taskID)
                            {
                                zadanie = new JTTT() { jesliTo = x, toTo = y };
                                list.Add(zadanie);
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
           

            return list;
        }
        public static void DodajZadanie(JTTT zad)
        {
            try
            {
                using (var ctx = new BazaContext())
                {
                    if (typeof(ZnajdzObraz).IsAssignableFrom(zad.jesliTo.GetType()))
                    {
                        ctx.ZnajdzObraz.Add((ZnajdzObraz)zad.jesliTo);
                    }
                    else if (typeof(SprawdzPogode).IsAssignableFrom(zad.jesliTo.GetType()))
                    {
                        ctx.SprawdzPogode.Add((SprawdzPogode)zad.jesliTo);
                    }

                    if(typeof(WyslijMaila).IsAssignableFrom(zad.toTo.GetType()))
                    {
                        ctx.WyslijMaila.Add((WyslijMaila)zad.toTo);
                    }
                    else if(typeof(PokazWOknie).IsAssignableFrom(zad.toTo.GetType()))
                    {
                        ctx.PokazWOknie.Add((PokazWOknie)zad.toTo);
                    }

                    ctx.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static void Czysc(){
            try
            {
                using (var ctx = new BazaContext())
                {
                    foreach (var x in ctx.ZnajdzObraz)
                    {
                        ctx.ZnajdzObraz.Remove(x);
                    }
                    foreach (var x in ctx.SprawdzPogode)
                    {
                        ctx.SprawdzPogode.Remove(x);
                    }
                    foreach (var x in ctx.WyslijMaila)
                    {
                        ctx.WyslijMaila.Remove(x);
                    }
                    foreach (var x in ctx.PokazWOknie)
                    {
                        ctx.PokazWOknie.Remove(x);
                    }
                    ctx.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
    }

    public class BazaContext : DbContext
    {
        public BazaContext()
            : base("JTTT-DataBase-JBKB")
        {
            Database.SetInitializer<BazaContext>(new BazaInit());
        }

        public DbSet<ZnajdzObraz> ZnajdzObraz { get; set; }
        public DbSet<SprawdzPogode> SprawdzPogode { get; set; }
        public DbSet<WyslijMaila> WyslijMaila { get; set; }
        public DbSet<PokazWOknie> PokazWOknie { get; set; }
    }

    public class BazaInit : DropCreateDatabaseIfModelChanges<BazaContext>
    {
        protected override void Seed(BazaContext context)
        {
            context.ZnajdzObraz.Add(new ZnajdzObraz() { haslo = "a", url = "http://demotywatory.pl/", taskID = 1 });
            context.PokazWOknie.Add(new PokazWOknie() { taskID = 1 });
            context.SprawdzPogode.Add(new SprawdzPogode() { miasto = "wroclaw", minTemp = 0,taskID=2});
            context.WyslijMaila.Add(new WyslijMaila() { mail = "xyarn2@gmail.com", taskID = 2 });
        
            context.SaveChanges();
        }
    }

}
