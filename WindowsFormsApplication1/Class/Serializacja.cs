﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    class Serializacja
    {
        BinaryFormatter formatter;
        string filename;

        public Serializacja(string nazwa)
        {
            filename = nazwa;
            formatter = new BinaryFormatter();

        }

        public void Serializuj(BindingList<JTTT> toSerialize)
        {

            FileStream file = new FileStream(filename, FileMode.Create);

            try
            {
                formatter.Serialize(file, toSerialize);
            }
            catch (SerializationException e)
            {

                MessageBox.Show("Blad Serializacji:\n " + e.Message);
            }
            finally
            {
                file.Close();
            }
        }
        public BindingList<JTTT> Deserializuj()
        {

            FileStream file = new FileStream(filename, FileMode.Open);
            BindingList<JTTT> lista = new BindingList<JTTT>();

            try
            {
                lista = (BindingList<JTTT>)formatter.Deserialize(file);
            }
            catch (SerializationException e)
            {
                MessageBox.Show("Blad: " + e.Message);
            }
            finally
            {
                file.Close();
            }
            return lista;
        }
    }
}
