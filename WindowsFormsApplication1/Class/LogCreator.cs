﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Log
    {
        public string tresc
        {
            set { _tresc = value; }
            get
            {
                if (_tresc == null)
                    return "Nieznane Wydarzenie";
                return tresc;
            }
        }
        private string _tresc;
        private DateTime czas;
        public Log()
        {
            czas = DateTime.Now;
        }
        public override string ToString()
        {
            return czas.ToString() + " :: " + _tresc;
        }

    }

    class LogCreator
    {
        static LogCreator singleton;

        StreamWriter file;
        string filename;
        int i;

        static private void Inicjalizuj()
        {
            singleton = new LogCreator();
        }

        public LogCreator()
        {

            filename = "jttt.log";
            file = new StreamWriter(filename, true);
            i = 1;
            file.WriteLine("Log wykonanych operacji w programie");
            file.Flush();
        }

        ~LogCreator()
        {
            //file.Dispose();
        }

        static public void Log(BindingList<Log> logList, string text)
        {
            if (singleton == null)
            {
                Inicjalizuj();
            }
            Log log = new Log() { tresc = text };
            singleton.file.WriteLine(singleton.i + " : " + log);
            singleton.file.Flush();
            if (logList != null)
            {
                logList.Add(log);
                Form1.singleton.AutoScrollLogList();
            }
            
        }

    }
}
