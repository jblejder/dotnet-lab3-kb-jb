﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    [Serializable]
    public class Paczka
    {
        public string textData { get; set; }
        public byte[] image { get; set; }
    }
}
