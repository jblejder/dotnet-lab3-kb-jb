﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using HtmlAgilityPack;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ComponentModel;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    [Serializable]
    public class JTTT
    {
        public int Id { get; set; }
        public string nazwa { get; set; }
        public IZadanie jesliTo { get; set; }
        public IZadanie toTo { get; set; }

        public bool Wykonaj()
        {

            try
            {
                if (!jesliTo.RobRobote())
                {
                    return false;
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }

            toTo.dane = jesliTo.dane;

            try
            {
                toTo.RobRobote();
            }
            catch (Exception exc)
            {
                throw exc;
            }



            return true;
        }
        public override string ToString()
        {
            return nazwa + " JT: \"" + jesliTo.ToString() + "\" TT: \"" + toTo.ToString() + "\"";
        }
    }
}
