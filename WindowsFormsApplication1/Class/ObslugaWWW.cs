﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WindowsFormsApplication1
{
    class ObslugaWWW
    {
        WebClient webClient;
        public String storedStringHtml;
        HtmlAgilityPack.HtmlDocument htmlDoc;
        public string foundAdress;


        public ObslugaWWW()
        {
            webClient = new WebClient();
            htmlDoc = new HtmlAgilityPack.HtmlDocument();
        }

        public void LoadHtml(string url)
        {
            if (url == null)
                return;
            storedStringHtml = webClient.DownloadString(url);
            htmlDoc.LoadHtml(storedStringHtml);
        }

        public bool FindByAlt(string lookingFor)
        {
            if (htmlDoc == null || lookingFor == null)
                return false;

            var nodes = htmlDoc.DocumentNode.Descendants("img");
            foreach (var x in nodes)
            {
                if (x.Attributes["alt"].Value.Contains(lookingFor))
                {
                    foundAdress = x.Attributes["src"].Value;
                }
            }
            return true;
        }

        public byte[] DownloadFoundFile()
        {
            if (foundAdress == null)
                return null;
            try
            {
                byte[] obraz = webClient.DownloadData(foundAdress);
                return obraz;
            }
            catch (WebException e)
            {
                throw e;
            }
        }
    }
}
