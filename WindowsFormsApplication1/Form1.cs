﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace WindowsFormsApplication1
{

    public partial class Form1 : Form
    {
        public static Form1 singleton;

        BindingList<Log> logLista = new BindingList<Log>();
        BindingList<JTTT> naszalista;
        Serializacja ser = new Serializacja("serializacja");
        bool bazaAktywna = true;

        public Form1()
        {
            singleton = this;
            InitializeComponent();
            LogListBox.DataSource = logLista;
            try
            {
                naszalista = BazaDanych.OdczytajElementy();
                LogCreator.Log(logLista, "Pobrano zadania z bazy danych");
            }
            catch (Exception e)
            {
                bazaAktywna = false;
                MessageBox.Show("U mnie dziala! :(\n\n\n" + e.Message);
                LogCreator.Log(logLista, "Blad pobrania danych z bazy - baza wylaczona");
                naszalista = new BindingList<JTTT>();
            }
            listBox1.DataSource = naszalista;
        }

        private void DodajDoListy_Click(object sender, EventArgs e)
        {
            JTTT zad = new JTTT() { nazwa = nazwaBox.Text };
            switch (JezeliTo.SelectedIndex)
            {
                case 0:
                    zad.jesliTo = new ZnajdzObraz() { haslo = stringBox.Text, url = WWWBox.Text, taskID = naszalista.Count + 1 };
                    break;
                case 1:
                    zad.jesliTo = new SprawdzPogode() { miasto = MiastoBox.Text, taskID = naszalista.Count + 1, minTemp = (int)NumberBox.Value };
                    break;
            }
            switch (ToTo.SelectedIndex)
            {
                case 0:
                    zad.toTo = new WyslijMaila() { mail = EmailBox.Text, udawajZeWysylasz = UdawajZeWysylasz.Checked, taskID = naszalista.Count + 1 };
                    break;
                case 1:
                    zad.toTo = new PokazWOknie() { taskID = naszalista.Count + 1 };
                    break;
            }

            LogCreator.Log(logLista, "Dodano zadanie: " + zad.ToString());
            naszalista.Add(zad);
            if (bazaAktywna)
                try
                {
                    BazaDanych.DodajZadanie(zad);
                    LogCreator.Log(logLista, "Zadanie dodane do bazy");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    LogCreator.Log(logLista, "Blad dodania zadania do bazy - baza wylaczona");
                    bazaAktywna = false;
                }


        }

        private void Wykonaj_Click(object sender, EventArgs e)
        {
            LogCreator.Log(logLista, "Wykonywanie zadan w osobnym watku");
            Wykonaj.Text = "Pracuje!";
            Wykonaj.Enabled = false;
            Thread thread = new Thread(() => Obliczenia(this));
            thread.Start();
        }

        private void Czysc_Click(object sender, EventArgs e)
        {
            naszalista.Clear();
            LogCreator.Log(logLista, "Lista wyczyszczona");
            if (bazaAktywna)
                try
                {
                    BazaDanych.Czysc();
                    LogCreator.Log(logLista, "Baza wyczyszczona");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    LogCreator.Log(logLista, "Blad czyszczenia bazy - baza wylaczona");
                    bazaAktywna = false;
                }
        }

        private void Serializuj_Click(object sender, EventArgs e)
        {
            try
            {
                ser.Serializuj(naszalista);
                LogCreator.Log(logLista, "Zadania zserializowane");
            }
            catch (Exception ex)
            {
                LogCreator.Log(logLista, "Blad serializacji: " + ex.Message);
            }
        }

        private void Deserializuj_Click(object sender, EventArgs e)
        {
            try
            {
                naszalista = ser.Deserializuj();
                listBox1.DataSource = naszalista;
                foreach (var x in naszalista)
                {
                    BazaDanych.DodajZadanie(x);
                }
                LogCreator.Log(logLista, "Zadania zdeserializowane");
            }
            catch (Exception ex)
            {
                LogCreator.Log(logLista, "Blad deserializacji: " + ex.Message);
            }

        }

        //Metody uzywane przez inny watek by moc zmieniac graficzny interfejs
        private void AktywujPrzycisk()
        {
            Wykonaj.Enabled = true;
            Wykonaj.Text = "Wykonaj!";
        }

        private void MoveProgressBar(int val)
        {
            ProgBar.Value = val;
        }

        private void Obliczenia(Form form)
        {

            int stepVal = 100 / naszalista.Count;
            int val = 0;
            foreach (var zad in naszalista)
            {
                try
                {
                    if (zad.Wykonaj())
                        singleton.Invoke(new MethodInvoker(() => LogCreator.Log(singleton.logLista, "Zad wykonane: " + zad.ToString())));
                    else
                        singleton.Invoke(new MethodInvoker(() => LogCreator.Log(singleton.logLista, "Nie spelnione warunki: " + zad.ToString())));
                }
                catch (Exception exc)
                {
                    singleton.Invoke(new MethodInvoker(() => LogCreator.Log(singleton.logLista, "Blad zadania: " + zad.ToString() + " Powod: " + exc.Message)));
                }

                val += stepVal;
                singleton.Invoke(new MethodInvoker(() => MoveProgressBar(val)));
            }


            singleton.Invoke(new MethodInvoker(() => AktywujPrzycisk()));
            singleton.Invoke(new MethodInvoker(() => MoveProgressBar(0)));
        }
        public void AutoScrollLogList()
        {
            LogListBox.SelectedIndex = LogListBox.Items.Count - 1;
            LogListBox.SelectedIndex = -1;
        }

    }
}
