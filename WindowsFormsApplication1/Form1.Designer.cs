﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda wsparcia projektanta - nie należy modyfikować
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.WWWBox = new System.Windows.Forms.TextBox();
            this.DodajDoListy = new System.Windows.Forms.Button();
            this.EmailBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.stringBox = new System.Windows.Forms.TextBox();
            this.UdawajZeWysylasz = new System.Windows.Forms.CheckBox();
            this.Wykonaj = new System.Windows.Forms.Button();
            this.Czysc = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.JezeliTo = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.NumberBox = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.MiastoBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ToTo = new System.Windows.Forms.TabControl();
            this.WyslijEmail = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nazwaBox = new System.Windows.Forms.TextBox();
            this.Deserializuj = new System.Windows.Forms.Button();
            this.Serializuj = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.ProgBar = new System.Windows.Forms.ProgressBar();
            this.LogListBox = new System.Windows.Forms.ListBox();
            this.JezeliTo.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumberBox)).BeginInit();
            this.ToTo.SuspendLayout();
            this.WyslijEmail.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // WWWBox
            // 
            this.WWWBox.AccessibleName = "";
            this.WWWBox.BackColor = System.Drawing.SystemColors.Window;
            this.WWWBox.Location = new System.Drawing.Point(60, 29);
            this.WWWBox.Name = "WWWBox";
            this.WWWBox.Size = new System.Drawing.Size(394, 20);
            this.WWWBox.TabIndex = 0;
            this.WWWBox.Text = "http://demotywatory.pl/";
            // 
            // DodajDoListy
            // 
            this.DodajDoListy.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.DodajDoListy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.DodajDoListy.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DodajDoListy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DodajDoListy.Location = new System.Drawing.Point(24, 391);
            this.DodajDoListy.Name = "DodajDoListy";
            this.DodajDoListy.Size = new System.Drawing.Size(163, 53);
            this.DodajDoListy.TabIndex = 1;
            this.DodajDoListy.Text = "Dodaj do listy";
            this.DodajDoListy.UseVisualStyleBackColor = false;
            this.DodajDoListy.Click += new System.EventHandler(this.DodajDoListy_Click);
            // 
            // EmailBox
            // 
            this.EmailBox.Location = new System.Drawing.Point(65, 32);
            this.EmailBox.Name = "EmailBox";
            this.EmailBox.Size = new System.Drawing.Size(389, 20);
            this.EmailBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(422, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Na danej stronie znajduje się obrazek, którego opis zawiera tekst:";
            // 
            // stringBox
            // 
            this.stringBox.Location = new System.Drawing.Point(60, 61);
            this.stringBox.Name = "stringBox";
            this.stringBox.Size = new System.Drawing.Size(394, 20);
            this.stringBox.TabIndex = 6;
            // 
            // UdawajZeWysylasz
            // 
            this.UdawajZeWysylasz.AutoSize = true;
            this.UdawajZeWysylasz.Location = new System.Drawing.Point(65, 58);
            this.UdawajZeWysylasz.Name = "UdawajZeWysylasz";
            this.UdawajZeWysylasz.Size = new System.Drawing.Size(127, 17);
            this.UdawajZeWysylasz.TabIndex = 8;
            this.UdawajZeWysylasz.Text = "Symulacja Wysyłania";
            this.UdawajZeWysylasz.UseVisualStyleBackColor = true;
            // 
            // Wykonaj
            // 
            this.Wykonaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Wykonaj.Location = new System.Drawing.Point(540, 198);
            this.Wykonaj.Name = "Wykonaj";
            this.Wykonaj.Size = new System.Drawing.Size(100, 60);
            this.Wykonaj.TabIndex = 10;
            this.Wykonaj.Text = "Wykonaj!";
            this.Wykonaj.UseVisualStyleBackColor = true;
            this.Wykonaj.Click += new System.EventHandler(this.Wykonaj_Click);
            // 
            // Czysc
            // 
            this.Czysc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Czysc.Location = new System.Drawing.Point(646, 198);
            this.Czysc.Name = "Czysc";
            this.Czysc.Size = new System.Drawing.Size(100, 60);
            this.Czysc.TabIndex = 11;
            this.Czysc.Text = "Czyść!";
            this.Czysc.UseVisualStyleBackColor = true;
            this.Czysc.Click += new System.EventHandler(this.Czysc_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.Location = new System.Drawing.Point(517, 19);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBox1.Size = new System.Drawing.Size(658, 173);
            this.listBox1.TabIndex = 12;
            // 
            // JezeliTo
            // 
            this.JezeliTo.Controls.Add(this.tabPage1);
            this.JezeliTo.Controls.Add(this.tabPage2);
            this.JezeliTo.Location = new System.Drawing.Point(20, 51);
            this.JezeliTo.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.JezeliTo.Name = "JezeliTo";
            this.JezeliTo.SelectedIndex = 0;
            this.JezeliTo.Size = new System.Drawing.Size(491, 141);
            this.JezeliTo.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.WWWBox);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.stringBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(483, 115);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Sprawdz Stronę";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(22, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Tekst:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(22, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "URL:";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.MenuBar;
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.NumberBox);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.MiastoBox);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(483, 115);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Sprawdz pogode";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 61);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Temp.:";
            // 
            // NumberBox
            // 
            this.NumberBox.Location = new System.Drawing.Point(53, 59);
            this.NumberBox.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.NumberBox.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            -2147483648});
            this.NumberBox.Name = "NumberBox";
            this.NumberBox.Size = new System.Drawing.Size(86, 20);
            this.NumberBox.TabIndex = 24;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 35);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Miasto:";
            // 
            // MiastoBox
            // 
            this.MiastoBox.Location = new System.Drawing.Point(53, 32);
            this.MiastoBox.Name = "MiastoBox";
            this.MiastoBox.Size = new System.Drawing.Size(305, 20);
            this.MiastoBox.TabIndex = 1;
            this.MiastoBox.Text = "Wroclaw";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label10.Location = new System.Drawing.Point(6, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(453, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "Sprawdz czy pogoda w danym polskim miescie jest wieksza niz podana";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.25F);
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 39);
            this.label4.TabIndex = 15;
            this.label4.Text = "Jeżeli to:";
            // 
            // ToTo
            // 
            this.ToTo.Controls.Add(this.WyslijEmail);
            this.ToTo.Controls.Add(this.tabPage4);
            this.ToTo.Location = new System.Drawing.Point(20, 237);
            this.ToTo.Name = "ToTo";
            this.ToTo.SelectedIndex = 0;
            this.ToTo.Size = new System.Drawing.Size(491, 124);
            this.ToTo.TabIndex = 16;
            // 
            // WyslijEmail
            // 
            this.WyslijEmail.BackColor = System.Drawing.SystemColors.MenuBar;
            this.WyslijEmail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.WyslijEmail.Controls.Add(this.label2);
            this.WyslijEmail.Controls.Add(this.label8);
            this.WyslijEmail.Controls.Add(this.EmailBox);
            this.WyslijEmail.Controls.Add(this.UdawajZeWysylasz);
            this.WyslijEmail.Location = new System.Drawing.Point(4, 22);
            this.WyslijEmail.Name = "WyslijEmail";
            this.WyslijEmail.Padding = new System.Windows.Forms.Padding(3);
            this.WyslijEmail.Size = new System.Drawing.Size(483, 98);
            this.WyslijEmail.TabIndex = 0;
            this.WyslijEmail.Text = "Wyślij Email";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Adres:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(164, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Wyślij wiadomość na adres email:";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.MenuBar;
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(483, 98);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Pokaz w oknie";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label12.Location = new System.Drawing.Point(77, 34);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(309, 31);
            this.label12.TabIndex = 0;
            this.label12.Text = "Po prostu pokaz w oknie";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.25F);
            this.label7.Location = new System.Drawing.Point(17, 195);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 39);
            this.label7.TabIndex = 17;
            this.label7.Text = "to to:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 368);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Nazwa zadania:";
            // 
            // nazwaBox
            // 
            this.nazwaBox.Location = new System.Drawing.Point(123, 365);
            this.nazwaBox.Name = "nazwaBox";
            this.nazwaBox.Size = new System.Drawing.Size(309, 20);
            this.nazwaBox.TabIndex = 19;
            // 
            // Deserializuj
            // 
            this.Deserializuj.Location = new System.Drawing.Point(752, 229);
            this.Deserializuj.Name = "Deserializuj";
            this.Deserializuj.Size = new System.Drawing.Size(107, 29);
            this.Deserializuj.TabIndex = 20;
            this.Deserializuj.Text = "Deserializuj";
            this.Deserializuj.UseVisualStyleBackColor = true;
            this.Deserializuj.Click += new System.EventHandler(this.Deserializuj_Click);
            // 
            // Serializuj
            // 
            this.Serializuj.Location = new System.Drawing.Point(752, 198);
            this.Serializuj.Name = "Serializuj";
            this.Serializuj.Size = new System.Drawing.Size(107, 29);
            this.Serializuj.TabIndex = 21;
            this.Serializuj.Text = "Serializuj";
            this.Serializuj.UseVisualStyleBackColor = true;
            this.Serializuj.Click += new System.EventHandler(this.Serializuj_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label9.Location = new System.Drawing.Point(931, 427);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(244, 20);
            this.label9.TabIndex = 22;
            this.label9.Text = "Jakub Blejder i Karolina Brezak";
            // 
            // ProgBar
            // 
            this.ProgBar.Location = new System.Drawing.Point(213, 421);
            this.ProgBar.Name = "ProgBar";
            this.ProgBar.Size = new System.Drawing.Size(658, 23);
            this.ProgBar.TabIndex = 23;
            // 
            // LogListBox
            // 
            this.LogListBox.FormattingEnabled = true;
            this.LogListBox.HorizontalScrollbar = true;
            this.LogListBox.Location = new System.Drawing.Point(540, 264);
            this.LogListBox.Name = "LogListBox";
            this.LogListBox.Size = new System.Drawing.Size(635, 147);
            this.LogListBox.TabIndex = 24;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(1187, 456);
            this.Controls.Add(this.LogListBox);
            this.Controls.Add(this.ProgBar);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Serializuj);
            this.Controls.Add(this.Deserializuj);
            this.Controls.Add(this.nazwaBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ToTo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.JezeliTo);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.Czysc);
            this.Controls.Add(this.Wykonaj);
            this.Controls.Add(this.DodajDoListy);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "Jezeli To To To by Karolina Brezak i Jakub Blejder";
            this.JezeliTo.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumberBox)).EndInit();
            this.ToTo.ResumeLayout(false);
            this.WyslijEmail.ResumeLayout(false);
            this.WyslijEmail.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox WWWBox;
        private System.Windows.Forms.Button DodajDoListy;
        private System.Windows.Forms.TextBox EmailBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox stringBox;
        private System.Windows.Forms.CheckBox UdawajZeWysylasz;
        private System.Windows.Forms.Button Wykonaj;
        private System.Windows.Forms.Button Czysc;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TabControl JezeliTo;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl ToTo;
        private System.Windows.Forms.TabPage WyslijEmail;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox nazwaBox;
        private System.Windows.Forms.Button Deserializuj;
        private System.Windows.Forms.Button Serializuj;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox MiastoBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown NumberBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ProgressBar ProgBar;
        private System.Windows.Forms.ListBox LogListBox;
    }
}

