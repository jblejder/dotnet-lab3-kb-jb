﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        public Paczka wazneDane { get; set; }

        public Form2()
        {
            InitializeComponent();
        }

        public void WprowadzWazneDane(Paczka dane)
        {
            wazneDane = dane;
            textBox.Text = wazneDane.textData;
            MemoryStream ms = new MemoryStream(wazneDane.image);
            ImageBox.Image = Image.FromStream(ms);
        }
    }
}
