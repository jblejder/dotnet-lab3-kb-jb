﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SprawdzCzyPogodaDziala
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Przycisk_Click(object sender, EventArgs e)
        {
            Przycisk.Text = "Ładuje...";
            Przycisk.Enabled = false;

            Pogoda pogoda = new Pogoda();
            MemoryStream ms = new MemoryStream();

            string miasto = MiastoBox.Text;
            try
            {
                pogoda = PogodaDownloader.PobierzPogode(miasto);
                ms = new MemoryStream(PogodaDownloader.PobierzObrazek(pogoda));
            }
            catch (Exception exc)
            {
                PogodaBox.Text = "Cos poszlo nie tak\n\n" + exc.Message;
            }
            finally
            {
                PogodaBox.Text = miasto + "\nPredkosc wiatru: " + pogoda.wind.speed + "\nTemp: " + (pogoda.main.temp - 272.15) + "\nCisnienie: " + pogoda.main.pressure;

                Image obraz;
                
                obraz = Image.FromStream(ms);
                ObrazBox.Image = obraz;
            }
            Przycisk.Enabled = true;
            Przycisk.Text = "Pobierz pogode";
        }
    }
}
