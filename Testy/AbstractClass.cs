﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testy
{
    interface IZadanie
    {
        void Rob();
    }

    class WyswietlKuba : IZadanie
    {
        public void Rob()
        {
            Console.WriteLine("Kuba");
        }
    }
    class WyswietlMarta : IZadanie
    {
        public void Rob()
        {
            Console.WriteLine("Marta");
        }
    }

    class WyswietlTraktor : IZadanie
    {
        public void Rob()
        {
            Console.WriteLine("Traktor");
        }
    }

    class JTTT
    {
        public IZadanie JT { get; set; }
        public IZadanie TT { get; set; }

        public void Wykonaj()
        {
            if (JT == null || TT == null)
            {
                Console.WriteLine("Niepelne");
            }
            else
            {
                Console.WriteLine("---");
                JT.Rob();
                TT.Rob();
                Console.WriteLine(JT.GetType());
                if (typeof(WyswietlKuba).IsAssignableFrom(JT.GetType()))
                {
                    Console.WriteLine("oh heay");
                }
            }
        }
    }
}

