﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace Testy
{

    class Program
    {
        static void Main(string[] args)
        {
            JTTT zad1 = new JTTT()
            {
                JT = new WyswietlKuba(),
                TT = new WyswietlMarta()
            };
            JTTT zad2 = new JTTT()
            {
                JT = new WyswietlTraktor(),
                TT = new WyswietlMarta()
            };
            JTTT zad3 = new JTTT()
            {
                TT = new WyswietlKuba(),
                JT = new WyswietlMarta()
            };
            zad1.Wykonaj();
            zad2.Wykonaj();
            zad3.Wykonaj();

            List<JTTT> zadania = new List<JTTT>();

            zadania.Add(zad1);
            zadania.Add(zad2);
            zadania.Add(zad3);

            StreamWriter SW = new StreamWriter(@"D:\JSON.json");
            SW.Write(JsonConvert.SerializeObject(zadania));
            SW.Close();

            Console.ReadKey();
        }
    }
}
